//Quiz

1. What directive is used by Node.js in loading the modules it needs?
	-runtime environment (RTE)
2. What Node.js module contains a method for server creation?
	-http
3. What is the method of the http object responsible for creating a server using Node.js?
	-createServer()
4. Where will console.log() output its contents when run in Node.js?
	-Terminal